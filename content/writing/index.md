---
title: "Writing Appearing on Other Sites"
date: 2022-09-11T00:00:00-10:00
draft: false
---

Besides here on [my blog](/post/), I've done some freelance writing. Enjoy!

## Technology

#### [Baeldung.com Linux Administration Guides](https://www.baeldung.com/linux/author/allananderson)
Tutorial site Baeldung hosts some of my Linux and infrastructure guides. Some of my favorites:

- [Guide to Linux Filesystems](https://www.baeldung.com/linux/filesystems)
- [Docker and Containers vs. Virtual Machines](https://www.baeldung.com/cs/containers-vs-virtual-machines) 

#### [Build It Today](https://builtation.substack.com/)
I started this project in the Fall of 2021, writing every week, examining software development from the perspective of a mostly self-taught build and release engineer. I have more to say, so I hope to get back to it soon!

## Game Criticism and Analysis
Just-for-fun very occasional writing about games and narrative.
- [What's In a Game](https://gamecrit.substack.com/p/whats-in-a-game-cbc254a9c857): The Longest Journey
